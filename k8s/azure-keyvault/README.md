Azure Key Vault FlexVolume for Kubernetes - Integrates Azure Key Vault with Kubernetes via a FlexVolume.

With the Azure Key Vault FlexVolume, developers can access application-specific secrets, keys, and certs stored in Azure Key Vault directly from their pods.

Ref. https://github.com/Azure/kubernetes-keyvault-flexvol


1. Installed the KeyVault FlexVolume: kubectl create -f kv_flexvol_installer.yaml
2. Created a SP and gave that SP read access to the keyvault and created a sp key.
3. Created an kubernetes secret with the SP credentisl above: kubectl create secret generic kvcreds --from-literal clientid=<CLIENTID> --from-literal clientsecret=<CLIENTSECRET> --type=azure/kv
4.  
