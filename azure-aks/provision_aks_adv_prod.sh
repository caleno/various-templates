#!/bin/bash

set -ex

az aks create --name ita-prod-w3-kube \
--resource-group ita-prod-w3 \
--location northeurope \
--kubernetes-version 1.11.3 \
--service-principal fcae76ed-15ad-4772-a539-dc08cb1fc71d \
--client-secret MAKE NEW KEY OR CHECK VAULT  \
--node-vm-size Standard_B2ms \
--node-count 2 \
--node-osdisk-size 30 \
--admin-username uibit \
--network-plugin azure \
--docker-bridge-address 172.17.0.1/16 \
--dns-service-ip 10.100.0.53 \
--service-cidr 10.100.0.0/20 \
--max-pods 30 \
--vnet-subnet-id /subscriptions/1bb61fc5-eebc-4888-b659-bfae14b94927/resourceGroups/ita-prod-w3/providers/Microsoft.Network/virtualNetworks/ita-prod-w3-vnet/subnets/kubenet
