#!/bin/bash

set -ex

az aks create --name ita-test-kube \
--resource-group ita-test-kube \
--location northeurope \
--kubernetes-version 1.11.2 \
--node-vm-size Standard_B2ms \
--node-count 1 \
--node-osdisk-size 30 \
--admin-username uibit 
