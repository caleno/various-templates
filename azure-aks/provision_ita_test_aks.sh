#!/bin/bash

set -ex

az aks create --name ita-test-kube \
--resource-group ita-test-kube \
--location northeurope \
--kubernetes-version 1.11.2 \
--service-principal  \
--client-secret  \
--node-vm-size Standard_B2ms \
--node-count 2 \
--node-osdisk-size 30 \
--admin-username uibit \
--dns-name-prefix uibit-kube \
--network-plugin azure \
--docker-bridge-address 172.17.0.1/16 \
--dns-service-ip 10.100.0.53 \
--service-cidr 10.100.0.0/21 \
--max-pods 30 \
--vnet-subnet-id /subscriptions/1bb61fc5-eebc-4888-b659-bfae14b94927/resourceGroups/ita-test-kube/providers/Microsoft.Network/virtualNetworks/ita-test-kubeVNET/subnets/kubenet
